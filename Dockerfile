FROM registry.gitlab.com/grahamcampbell/llvm:9-buster

ARG GHC_VERSION=8.10.2
ARG CABAL_VERSION=3.4.0.0-rc1
ARG STACK_VERSION=2.3.3

COPY linux-${GHC_VERSION}.json /root/linux-${GHC_VERSION}.json

RUN apt-get update && apt-get install -y --no-install-recommends libgmp-dev \
        libnuma-dev libsqlite3-dev libtinfo-dev libtinfo5 openssh-client \
        python3 zlib1g-dev && rm -rf /var/lib/apt/lists/*

RUN if [ `uname -m` = "aarch64" ] ; then \
        curl -SL https://downloads.haskell.org/~ghc/${GHC_VERSION}/ghc-${GHC_VERSION}-aarch64-deb10-linux.tar.xz -o ghc.tar.xz ; \
    else \
        curl -SL https://downloads.haskell.org/~ghc/${GHC_VERSION}/ghc-${GHC_VERSION}-x86_64-deb10-linux.tar.xz -o ghc.tar.xz ; \
    fi && \
    tar -xf ghc.tar.xz && \
    cd ghc-${GHC_VERSION} && \
    ./configure && \
    make install && \
    cd ../ && \
    rm -rf ghc.tar.xz ghc-${GHC_VERSION}

RUN curl -SL https://github.com/haskell/cabal/archive/cabal-install-${CABAL_VERSION}.tar.gz -o cabal.tar.gz && \
    tar -xf cabal.tar.gz && \
    cd cabal-cabal-install-${CABAL_VERSION} && \
    ./bootstrap/bootstrap.py -d /root/linux-${GHC_VERSION}.json && \
    mkdir /root/.cabal && \
    mv _build/bin /root/.cabal/bin && \
    cd ../ && \
    rm -rf cabal.tar.gz cabal-cabal-install-${CABAL_VERSION} /root/linux-${GHC_VERSION}.json

RUN if [ `uname -m` = "aarch64" ] ; then \
        curl -SL https://github.com/commercialhaskell/stack/releases/download/v${STACK_VERSION}/stack-${STACK_VERSION}-linux-aarch64.tar.gz -o stack.tar.gz ; \
    else \
        curl -SL https://github.com/commercialhaskell/stack/releases/download/v${STACK_VERSION}/stack-${STACK_VERSION}-linux-x86_64.tar.gz -o stack.tar.gz ; \
    fi && \
    tar -xf stack.tar.gz -C /usr/local/bin --strip-components=1 && \
    /usr/local/bin/stack config set system-ghc --global true && \
    /usr/local/bin/stack config set install-ghc --global false && \
    rm -rf stack.tar.gz

ENV PATH /root/.cabal/bin:/root/.local/bin:/opt/ghc/${GHC_VERSION}/bin:$PATH

CMD ["ghci"]
